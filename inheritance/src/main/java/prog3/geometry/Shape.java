package prog3.geometry;

public interface Shape {
    double getArea();
    double getPerimeter();
}
