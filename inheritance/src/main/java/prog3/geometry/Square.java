package prog3.geometry;

public class Square extends Rectangle{
    
    public Square(double side){
        super(side, side);
    }
}
