package prog3.geometry;

public class LotsOfShapes {
    
    public static void main(String[] args){
        Shape[] shapes = new Shape[5];
        shapes[0] = new Rectangle(3, 2);
        shapes[1] = new Rectangle(5, 3);
        shapes[2] = new Circle(3);
        shapes[3] = new Circle(3);
        shapes[4] = new Square(6);

        for(int i=0; i<shapes.length; i++){
            System.out.println(shapes[i].getArea());
            System.out.println(shapes[i].getPerimeter());
        }
    }
}
